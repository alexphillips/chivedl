let Command = require('node-climax').Command,
  Logger = require('node-climax').Logger,
  fs = require('fs'),
  url = require('url'),
  path = require('path'),
  Crawler = require('simplecrawler'),
  cheerio = require('cheerio'),
  Request = require('request'),
  async = require('async'),
  parser = require('xml2js').parseString;

class ChiveDownloadCommand extends Command {
  async run(args, options) {
    this.options = options;
    this.saveDir = ChiveDownloadCommand.trimPath(options['save-dir']) + '/';

    let limit = options.limit ? options.limit : 5;

    return new Promise((resolve, reject) => {
      Request.get('http://thechive.com/sitemap.xml', (err, response) => {
        return parser(response.body, (err, result) => {
          async.forEachSeries(result.sitemapindex.sitemap, (item, callback) => {
            Request.get(item.loc[0], (err, response) => {
              parser(response.body, (err, result) => {
                async.forEachLimit(result.urlset.url, limit, (pageInfo, callback) => {
                  if (!pageInfo.loc[0]) {
                    return callback();
                  }
                  this.scrape(pageInfo.loc[0], callback);
                }, err => {
                  return callback(err);
                });
              });
            });
          }, err => {
            if (err) {
              return reject(err);
            }

            Logger.info('Done.');
            resolve();
          });
        });
      });
    });
  }

  scrape(postUrl, callback) {
    if (postUrl.match(/theberry\.com|chivery\.com/)) {
      Logger.error('Excluding links from TheBerry');

      return callback();
    }

    Request.get(postUrl, (err, response) => {
      if (err) {
        Logger.error(`Error fetching post URL ${postUrl}`);

        return callback(err);
      }

      let $ = cheerio.load(response.body);
      let title = $('title').first().text();

      if ($('div.gallery-item-wrap').get().length === 0) {
        return callback();
      }

      if (this.options.category) {
        let skip = true;
        let foundCategories = [];
        let cardLabel = $('.article-header .card-categories a');
        if (cardLabel.length !== 0) {
          foundCategories.push(cardLabel.first().text());
          if (cardLabel.first().attr('href').match(new RegExp(this.options.category))) {
            skip = false;
          }
        } else {
          let singleCategories = $('.article-header .single-categories a').get();
          for (let i = 0; i < singleCategories.length; i++) {
            foundCategories.push($(singleCategories[i]).text());
            if ($(singleCategories[i]).attr('href').match(new RegExp(this.options.category))) {
              skip = false;
              break;
            }
          }
        }

        if (skip) {
          Logger.debug(`Skipping post with categories ${foundCategories.join(', ')}`)
          return callback();
        } else {
          Logger.debug(`Downloading post with categories ${foundCategories.join(', ')}`);
        }
      }

      Logger.info(title);

      let dlDir = `${this.saveDir}${ChiveDownloadCommand.slugify(title)} (${$('div.meta-left time').first().attr('datetime')})`;
      if (!fs.existsSync(dlDir)) {
        Logger.debug(`Creating directory ${dlDir}`);
        fs.mkdirSync(dlDir);
      } else {
        Logger.debug(`Directory ${dlDir} already exists.`);
      }

      async.forEachOfLimit($('div.gallery-item-wrap img').get(), 5, (el, index, callback) => {
        let $el = $(el);
        let ext = path.extname(url.parse($el.attr('src')).pathname);

        let saveFile = `${dlDir}/${index} - ${ChiveDownloadCommand.slugify($el.attr('alt'))}${ext}`;
        if ($el.attr('data-gifsrc')) {
          ext = path.extname(url.parse($el.attr('data-gifsrc')).pathname);
          saveFile = `${dlDir}/${index} - ${ChiveDownloadCommand.slugify($el.attr('data-gifsrc'))}${ext}`;
        }

        if (fs.existsSync(`${saveFile}`)) {
          return callback();
        }

        let stream = Request.get($el.attr('src'))
          .on('error', err => {
            if (err.statusCode === 404) {
              Logger.verbose(`URL ${$el.attr('src')} not found. Skipping.`);
              return callback();
            }

            return callback(err);
          })
          .pipe(fs.createWriteStream(`${saveFile}.dl`));
          stream.on('finish', err => {
            if (err) {
              return reject(err);
            }

            fs.renameSync(`${saveFile}.dl`, saveFile);

            callback();
          });
      }, err => {
        callback(err);
      });
    });
  }

  static trimPath(string) {
    while (string.charAt(string.length - 1) === '/') {
      string = string.substring(0, string.length - 1);
    }

    return string;
  }

  static slugify(text) {
    let retval = text.toString().toLowerCase()
      .replace(/\s+/g, '-')           // Replace spaces with -
      .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
      .replace(/\-\-+/g, '-')         // Replace multiple - with single -
      .replace(/^-+/, '')             // Trim - from start of text
      .replace(/([\uE000-\uF8FF]|\uD83C[\uDF00-\uDFFF]|\uD83D[\uDC00-\uDDFF])/g, '') // remove emojis
      .replace(/-+$/, '');            // Trim - from end of text

    if (retval.length > 200) {
      retval = retval.substring(0, 200) + '...';
    }

    return retval;
  }
}

module.exports = ChiveDownloadCommand;
