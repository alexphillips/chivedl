let App = require('node-climax').App;
(new App('chivedl'))
  .init({
    'crawl': {
      usage: '',
      desc: 'Crawl the entire site',
      options: {
        s: {
          alias: 'save-dir',
          demand: false,
          desc: 'Save directory',
          type: 'string',
        },
        c: {
          alias: 'category',
          demand: false,
          desc: 'Only download galleries within a category',
          type: 'string',
        },
        l: {
          alias: 'limit',
          demand: false,
          desc: 'Specify number of URLs to scrape at once [default: 5]',
          type: 'string',
        },
      },
      file: `${__dirname}/ChiveDownloadCommand`,
    },
  })
  .run();
